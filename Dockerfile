FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/paydaytrade-1.0.0.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/paydaytrade-1.0.0.jar"]
