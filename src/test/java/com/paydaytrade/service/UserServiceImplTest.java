package com.paydaytrade.service;

import com.paydaytrade.domain.User;
import com.paydaytrade.domain.VerificationToken;
import com.paydaytrade.dto.UserInfoDTO;
import com.paydaytrade.error.PaydayTradeException;
import com.paydaytrade.repository.UserRepository;
import com.paydaytrade.repository.VerificationTokenRepository;
import com.paydaytrade.util.Email;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class UserServiceImplTest {

    private static final long DUMMY_ID = 1;
    private static final String DUMMY_STRING = "String";
    private static final String DUMMY_PASSWORD = "Password1";
    private static final String USERNAME_ALREADY_USED_MESSAGE = "Username already used";
    private static final String USER_ALREADY_REGISTERED_MESSAGE = "User already registered with specified email";
    private static final String PASSWORD_WRONG_LENGTH_MESSAGE = "Password must contain six or more characters";
    private static final String PASSWORD_NOT_ALPHANUMERIC_MESSAGE = "Password must contain alphanumeric characters";
    private static final String INVALID_LINK_MESSAGE = "The link is invalid";
    private static final String NOT_FOUND_USER_MESSAGE = "User not found";


    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private VerificationTokenRepository verificationTokenRepository;

    @Mock
    private Email email;

    @Spy
    private ModelMapper modelMapper;


    private User user;
    private VerificationToken verificationToken;
    private UserInfoDTO userInfoDTO;


    @BeforeEach
    void setUp() {
        user = getUser();
        userInfoDTO = getUserInfoDTO();
        verificationToken = getVerificatonToken();
    }

    @Test
    void givenUserNamePresentWhenSignUpThenException() {
        when(userRepository.findByUserName(DUMMY_STRING)).thenReturn(Optional.of(user));

        assertThatThrownBy(() -> userService.signUp(userInfoDTO))
                .isInstanceOf(PaydayTradeException.class).hasMessage(USERNAME_ALREADY_USED_MESSAGE);
    }

    @Test
    void givenEmailFoundWhenSignUpThenException() {
        when(userRepository.findByEmailIgnoreCase(DUMMY_STRING)).thenReturn(Optional.of(user));

        assertThatThrownBy(() -> userService.signUp(userInfoDTO))
                .isInstanceOf(PaydayTradeException.class).hasMessage(USER_ALREADY_REGISTERED_MESSAGE);
    }

    @Test
    void givenPasswordLengthLessThanSixWhenSignUpThenException() {
        userInfoDTO.setPassword("pass");

        assertThatThrownBy(() -> userService.signUp(userInfoDTO))
                .isInstanceOf(PaydayTradeException.class).hasMessage(PASSWORD_WRONG_LENGTH_MESSAGE);
    }

    @Test
    void givenPasswordNotAlphaNumericWhenSignUpThenException() {
        userInfoDTO.setPassword(DUMMY_STRING);

        assertThatThrownBy(() -> userService.signUp(userInfoDTO))
                .isInstanceOf(PaydayTradeException.class).hasMessage(PASSWORD_NOT_ALPHANUMERIC_MESSAGE);
    }

    @Test
    void givenValidUserInfoDtoInputWhenSignUpThenExpectVerificationToken() {
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(verificationTokenRepository.save(any(VerificationToken.class))).thenReturn(verificationToken);
        MockedStatic<Email> emailMockedStatic = mockStatic(Email.class);
        String token = userService.signUp(userInfoDTO);
        emailMockedStatic.close();
        assertThat(token).isNotEmpty();
    }

    @Test
    void givenTokenNotPresentWhenConfirmUserTokenThenException() {
        when(verificationTokenRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.confirmUserToken(DUMMY_STRING))
                .isInstanceOf(PaydayTradeException.class).hasMessage(INVALID_LINK_MESSAGE);
    }

    @Test
    void givenEmailNotExistWhenConfirmUserTokenThenException() {
        when(verificationTokenRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.of(verificationToken));
        when(userRepository.findByEmailIgnoreCase(DUMMY_STRING)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.confirmUserToken(DUMMY_STRING))
                .isInstanceOf(PaydayTradeException.class).hasMessage(NOT_FOUND_USER_MESSAGE);
    }

    @Test
    void givenValidTokenWhenConfirmUserTokenThenExpectUserDto() {
        when(verificationTokenRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.of(verificationToken));
        when(userRepository.findByEmailIgnoreCase(DUMMY_STRING)).thenReturn(Optional.of(user));

        UserInfoDTO userInfo = userService.confirmUserToken(DUMMY_STRING);
        verify(userRepository, times(1)).save(this.user);
        assertThat(userInfo).isEqualTo(userInfoDTO);

    }

    @Test
    void givenPasswordLengthLessThanSixWhenLoginThenException() {
        String password = "pass";
        assertThatThrownBy(() -> userService.login(DUMMY_STRING, password))
                .isInstanceOf(PaydayTradeException.class).hasMessage(PASSWORD_WRONG_LENGTH_MESSAGE);
    }

    @Test
    void givenPasswordNotAlphaNumericWhenLoginThenException() {
        assertThatThrownBy(() -> userService.login(DUMMY_STRING, DUMMY_STRING))
                .isInstanceOf(PaydayTradeException.class).hasMessage(PASSWORD_NOT_ALPHANUMERIC_MESSAGE);
    }

    @Test
    void givenInvalidUsernameAndPasswordWhenLoginThenException() {
        when(userRepository.findByUserNameAndPasswordAndEnabledTrue(DUMMY_STRING, DUMMY_PASSWORD)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.login(DUMMY_STRING, DUMMY_PASSWORD))
                .isInstanceOf(PaydayTradeException.class).hasMessage(NOT_FOUND_USER_MESSAGE);
    }

    private User getUser() {
        return User
                .builder()
                .id(DUMMY_ID)
                .userName(DUMMY_STRING)
                .password(DUMMY_PASSWORD)
                .email(DUMMY_STRING)
                .build();
    }

    private UserInfoDTO getUserInfoDTO() {
        return UserInfoDTO
                .builder()
                .userName(DUMMY_STRING)
                .password(DUMMY_PASSWORD)
                .email(DUMMY_STRING)
                .build();
    }

    private VerificationToken getVerificatonToken() {
        return VerificationToken
                .builder()
                .user(getUser())
                .token(DUMMY_STRING)
                .id(DUMMY_ID)
                .build();
    }
}