package com.paydaytrade.service;

import com.paydaytrade.dto.RealTimeStockPriceDTO;

import java.util.List;

public interface StockDataService {

    List<RealTimeStockPriceDTO> readStock();
    RealTimeStockPriceDTO getStockDataBySymbol(String symbol);
    List<RealTimeStockPriceDTO> getCurrentStockDataList();
}
