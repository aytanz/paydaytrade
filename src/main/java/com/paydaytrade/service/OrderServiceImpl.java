package com.paydaytrade.service;


import com.paydaytrade.domain.Order;
import com.paydaytrade.domain.UserStock;
import com.paydaytrade.domain.enumeration.OrderType;
import com.paydaytrade.dto.EmailDTO;
import com.paydaytrade.dto.RealTimeStockPriceDTO;
import com.paydaytrade.error.PaydayTradeException;
import com.paydaytrade.repository.OrderRepository;
import com.paydaytrade.repository.StockRepository;
import com.paydaytrade.util.Email;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final StockRepository stockRepository;
    private final StockDataService stockDataService;


    public void fillBuyOrder() {
        List<Order> buyOrderList = orderRepository.findByOrderType(OrderType.BUY);
        buyOrderList.parallelStream().forEach(order -> {
            RealTimeStockPriceDTO realTimeStockPriceDTO = stockDataService.getCurrentStockDataList().stream()
                    .filter(currentStock -> currentStock.getSymbol()
                            .equals(order.getSymbol())).findAny().orElseThrow(() -> new PaydayTradeException("Stock Data not found with symbol"));
            if (order.getEstimatedPrice().compareTo(realTimeStockPriceDTO.getPrice()) >= 0) {
                createUserStock(order.getUser().getId(), realTimeStockPriceDTO.getSymbol(), realTimeStockPriceDTO.getPrice());
                orderRepository.deleteById(order.getId());

                sendEmail(order.getUser().getEmail(), order.getSymbol());

                //remove price from balance
            }
        });
    }

    public void fillSellOrder() {
        List<Order> sellOrderList = orderRepository.findByOrderType(OrderType.SELL);
        sellOrderList.parallelStream().forEach(order -> {
            RealTimeStockPriceDTO realTimeStockPriceDTO = stockDataService.getCurrentStockDataList().stream()
                    .filter(currentStock -> currentStock.getSymbol()
                            .equals(order.getSymbol())).findAny().orElseThrow(() -> new PaydayTradeException("Stock Data not found with symbol"));
            if (order.getEstimatedPrice().compareTo(realTimeStockPriceDTO.getPrice()) <= 0) {
                stockRepository.deleteBySymbolAndUserId(order.getSymbol(), order.getUser().getId());
                orderRepository.deleteById(order.getId());

                sendEmail(order.getUser().getEmail(), order.getSymbol());
            }
        });
    }

    private void createUserStock(long userId, String symbol, BigDecimal price) {
        UserStock userStock = new UserStock();
        userStock.setUserId(userId);
        userStock.setSymbol(symbol);
        userStock.setPrice(price);
        stockRepository.save(userStock);
    }

    private void sendEmail(String recipientEmail, String symbol) {
        String message = "Dear user. Your order (" + symbol + ") filled";

        EmailDTO emailDTO = EmailDTO.builder().recipient(recipientEmail)
                .subject("Complete Registration!")
                .message(message).build();

        Email.sendEmail(emailDTO);
    }

}
