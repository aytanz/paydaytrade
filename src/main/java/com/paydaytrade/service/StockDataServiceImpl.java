package com.paydaytrade.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paydaytrade.dto.RealTimeStockPriceDTO;
import com.paydaytrade.dto.StockDataDTO;
import com.paydaytrade.error.PaydayTradeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class StockDataServiceImpl implements StockDataService {


    private static List<RealTimeStockPriceDTO> currentStockDataList;
    private final ModelMapper modelMapper;

    public List<RealTimeStockPriceDTO> readStock() {
        String result;
        try {
            URL url = new URL("https://financialmodelingprep.com/api/v3/quotes/nyse?apikey=ab2bf347fd072714dbe65be68b93c286");
            try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = br.lines().collect(Collectors.joining("\n"));
            }
            ObjectMapper mapper = new ObjectMapper();
            List<StockDataDTO> currentStockDtoList = mapper.readValue(result, new TypeReference<>() {});

            currentStockDataList = currentStockDtoList.stream()
                    .map(stockDataDTO -> modelMapper.map(stockDataDTO, RealTimeStockPriceDTO.class))
                    .sorted(Comparator.comparing(RealTimeStockPriceDTO::getSymbol))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new PaydayTradeException(e.getMessage());
        }
        return currentStockDataList;
    }

    @Override
    public RealTimeStockPriceDTO getStockDataBySymbol(String symbol) {
        return currentStockDataList.stream()
                .filter(currentStock -> currentStock.getSymbol().equals(symbol))
                .findAny()
                .orElseThrow(() -> new PaydayTradeException("Stock data not found with specified symbol"));
    }

    public List<RealTimeStockPriceDTO> getCurrentStockDataList() {
        return currentStockDataList;
    }
}
