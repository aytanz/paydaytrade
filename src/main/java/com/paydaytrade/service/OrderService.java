package com.paydaytrade.service;

public interface OrderService {

    void fillBuyOrder();

    void fillSellOrder();
}
