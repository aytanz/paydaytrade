package com.paydaytrade.service;

import com.paydaytrade.domain.User;
import com.paydaytrade.domain.VerificationToken;
import com.paydaytrade.dto.EmailDTO;
import com.paydaytrade.dto.UserInfoDTO;
import com.paydaytrade.error.PaydayTradeException;
import com.paydaytrade.repository.UserRepository;
import com.paydaytrade.repository.VerificationTokenRepository;
import com.paydaytrade.util.Email;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final ModelMapper modelMapper;

    public String signUp(UserInfoDTO customerInfo) {

        validateUserInfo(customerInfo);

        User user = createUser(customerInfo);

        String token = generateVerificationToken(user);

        sendEmail(user.getEmail(), token);
        return token;
    }

    public UserInfoDTO confirmUserToken(String token) {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(token)
                .orElseThrow(() -> new PaydayTradeException("The link is invalid"));

        User user = userRepository.findByEmailIgnoreCase(verificationToken.getUser().getEmail())
                .orElseThrow(() -> new PaydayTradeException("User not found"));

        user.setEnabled(true);
        userRepository.save(user);

        return modelMapper.map(user, UserInfoDTO.class);
    }

    public User login(String userName, String password) {
        validatePassword(password);
        return userRepository.findByUserNameAndPasswordAndEnabledTrue(userName, password)
                .orElseThrow(() -> new PaydayTradeException("User not found"));
    }


    private User createUser(UserInfoDTO customerInfoDTO) {
        User user = modelMapper.map(customerInfoDTO, User.class);
        return userRepository.save(user);
    }

    private String generateVerificationToken(User user) {
        VerificationToken verificationToken = VerificationToken.builder()
                .user(user)
                .token(UUID.randomUUID().toString()).build();
        verificationTokenRepository.save(verificationToken);
        return verificationToken.getToken();
    }

    private void sendEmail(String recipientEmail, String token) {
        EmailDTO emailDTO = EmailDTO.builder().recipient(recipientEmail)
                .subject("Complete Registration!")
                .message("To confirm your account, please click here : "
                        + "http://localhost:8080/confirm?token=" + token).build();

        Email.sendEmail(emailDTO);
    }

    private void validateUserInfo(UserInfoDTO userInfo) {
        if (userRepository.findByUserName(userInfo.getUserName()).isPresent()) {
            throw new PaydayTradeException("Username already used");
        }
        if (userRepository.findByEmailIgnoreCase(userInfo.getEmail()).isPresent()) {
            throw new PaydayTradeException("User already registered with specified email");
        }
        validatePassword(userInfo.getPassword());
    }

    private void validatePassword(String password) {
        if (password.length() < 6) {
            throw new PaydayTradeException("Password must contain six or more characters");
        }
        if (!isAlphaNumeric(password)) {
            throw new PaydayTradeException("Password must contain alphanumeric characters");
        }
    }

    private static boolean isAlphaNumeric(String pass) {
        return pass != null && pass.chars().allMatch(Character::isLetterOrDigit)
                && pass.chars().anyMatch(Character::isLetter)
                && pass.chars().anyMatch(Character::isDigit)
                && pass.chars().anyMatch(Character::isLowerCase)
                && pass.chars().anyMatch(Character::isUpperCase);
    }
}
