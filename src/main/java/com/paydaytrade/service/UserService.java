package com.paydaytrade.service;

import com.paydaytrade.domain.User;
import com.paydaytrade.dto.UserInfoDTO;

public interface UserService {

    String signUp(UserInfoDTO customerInfo);
    UserInfoDTO confirmUserToken(String token);
    User login(String userName, String password);

}
