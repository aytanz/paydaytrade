package com.paydaytrade.repository;

import com.paydaytrade.domain.Order;
import com.paydaytrade.domain.enumeration.OrderType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByOrderType(OrderType orderType);

}
