package com.paydaytrade.repository;

import com.paydaytrade.domain.UserStock;
import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<UserStock, Long> {
    StockRepository deleteBySymbolAndUserId(String symbol, long userId);
}
