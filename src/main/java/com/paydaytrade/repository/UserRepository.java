package com.paydaytrade.repository;

import com.paydaytrade.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,Long> {

    Optional<User> findByUserName(String userName);

    Optional<User> findByUserNameAndPasswordAndEnabledTrue(String userName, String password);

    Optional<User> findByEmailIgnoreCase(String emailId);

}
