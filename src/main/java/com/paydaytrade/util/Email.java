package com.paydaytrade.util;

import com.paydaytrade.dto.EmailDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Slf4j
public class Email {

    @Value("${email.username}")
    private static String userName;

    @Value("${email.password}")
    private static String password;

    @Value("${email.from}")
    private static String from;

    @Value("${email.host}")
    private static String host;


    public static void sendEmail(EmailDTO emailDTO) {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(host);
        javaMailSender.setUsername(from);
        javaMailSender.setPassword(password);

        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", "true");
        mailProperties.put("mail.smtp.starttls.enable", "true");
        javaMailSender.setJavaMailProperties(mailProperties);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(emailDTO.getRecipient());
        email.setSubject(emailDTO.getSubject());
        email.setText(emailDTO.getMessage());
        javaMailSender.send(email);
        log.info("Email has been sent successfully");
    }
}
