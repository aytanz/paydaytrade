package com.paydaytrade.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDTO {

    private String message;
    private String token;
    private String recipient;
    private String subject;

}
