package com.paydaytrade.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RealTimeStockPriceDTO {

    private String symbol;
    private BigDecimal price;
    private Long timestamp;

}
