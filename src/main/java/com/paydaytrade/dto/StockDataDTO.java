package com.paydaytrade.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockDataDTO {
    private String symbol;
    private String name;
    private BigDecimal price;
    private BigDecimal changesPercentage;
    private BigDecimal change;
    private BigDecimal dayLow;
    private BigDecimal dayHigh;
    private BigDecimal yearHigh;
    private BigDecimal yearLow;
    private BigDecimal marketCap;
    private BigDecimal priceAvg50;
    private BigDecimal priceAvg200;
    private Long volume;
    private Long avgVolume;
    private String exchange;
    private BigDecimal open;
    private BigDecimal previousClose;
    private String eps;
    private String pe;
    private String earningsAnnouncement;
    private String sharesOutstanding;
    private Long timestamp;
}
