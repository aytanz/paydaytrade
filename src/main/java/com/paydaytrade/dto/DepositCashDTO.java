package com.paydaytrade.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class DepositCashDTO {
    private long userId;
    private Double amount;
    private String currency;
    private LocalDate addTime;
}
