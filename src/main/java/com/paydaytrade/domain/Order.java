package com.paydaytrade.domain;


import com.paydaytrade.domain.enumeration.OrderType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private long id;

    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    private String symbol;

    private BigDecimal estimatedPrice;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id")
    private User user;


}
