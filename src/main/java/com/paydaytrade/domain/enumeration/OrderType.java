package com.paydaytrade.domain.enumeration;

public enum OrderType {
    BUY, SELL;
}
