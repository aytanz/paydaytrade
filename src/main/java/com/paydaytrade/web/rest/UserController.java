package com.paydaytrade.web.rest;

import com.paydaytrade.domain.User;
import com.paydaytrade.service.UserService;
import com.paydaytrade.dto.UserInfoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/signup")
    public ResponseEntity<String> signUp(@RequestBody UserInfoDTO userInfo) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userService.signUp(userInfo));
    }

    @GetMapping("/confirm")
    public ResponseEntity<UserInfoDTO> confirmUserToken(@RequestParam String token) {
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(userService.confirmUserToken(token));
    }

    @GetMapping("/login")
    public ResponseEntity<User> login(@RequestParam String userName, @RequestParam String password){
        return ResponseEntity.ok(userService.login(userName, password));
    }
}
