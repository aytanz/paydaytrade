package com.paydaytrade.web.rest;

import com.paydaytrade.dto.RealTimeStockPriceDTO;
import com.paydaytrade.service.StockDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("stocks")
@RequiredArgsConstructor
public class StockDataController {

    private final StockDataService stockDataService;

    @GetMapping
    public ResponseEntity<List<RealTimeStockPriceDTO>> getRealTimeStockData(){
        return new ResponseEntity<>(stockDataService.getCurrentStockDataList(), null, HttpStatus.OK);
    }

    @GetMapping("/{symbol}")
    public ResponseEntity<RealTimeStockPriceDTO> getStockDataBySymbol(@PathVariable String symbol){
        return ResponseEntity.ok(stockDataService.getStockDataBySymbol(symbol));
    }

}
