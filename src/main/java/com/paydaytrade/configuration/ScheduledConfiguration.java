package com.paydaytrade.configuration;

import com.paydaytrade.service.OrderService;
import com.paydaytrade.service.StockDataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
public class ScheduledConfiguration implements SchedulingConfigurer {
    private final OrderService orderService;
    private final StockDataService stockDataService;

    private long duration = 1000L;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        log.info("Scheduled task started");
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(10);
        threadPoolTaskScheduler.initialize();
        getCurrentStockJob(threadPoolTaskScheduler);
        fillBuyOrderJob(threadPoolTaskScheduler);
        fillSellOrderJob(threadPoolTaskScheduler);
        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
    }

    private void getCurrentStockJob(TaskScheduler scheduler){
        scheduler.scheduleAtFixedRate(stockDataService::readStock, duration);
    }

    private void fillBuyOrderJob(TaskScheduler scheduler) {
        scheduler.scheduleAtFixedRate(orderService::fillBuyOrder, duration);
    }

    private void fillSellOrderJob(TaskScheduler scheduler) {
        scheduler.scheduleAtFixedRate(orderService::fillSellOrder, duration);
    }
}
