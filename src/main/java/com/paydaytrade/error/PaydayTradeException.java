package com.paydaytrade.error;

public class PaydayTradeException extends RuntimeException{

    public PaydayTradeException(String message) {
        super(message);
    }
}
